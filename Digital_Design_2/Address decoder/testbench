----------------------------------------------------------------------------------
-- Company: DRAMCO -- KU Leuven
-- Students: firstname lastname and other guy/girl/...
-- 
-- Module Name: peripheral_address_decoder_tb - Behavioral
-- Course Name: Lab Digital Design
-- 
-- Description: 
--  Test the peripheral_address_decoder module.
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use IEEE.math_real.all;

use IEEE.NUMERIC_STD.ALL;

library STD;
use STD.TEXTIO.ALL;

entity peripheral_address_decoder_tb is
end peripheral_address_decoder_tb;

architecture Behavioral of peripheral_address_decoder_tb is
    
    -- constants
    constant clk_period : time := 10 ns;
    signal C_BASE_ADDR : std_logic_vector(11 downto 0) := x"0F0";
    
    -- system clock (not needed because DUT is a combinatorial circuit, but timing can be derived from it)
    signal clk : std_logic := '0';
    
    type addresses_arr is array (0 to 3) of std_logic_vector(11 downto 0);
    
    constant base_addresses : addresses_arr := (x"100", x"0f0", x"850", x"300");
    
    type rw_outputs is array (base_addresses'range) of std_logic;
    type addr_peri_outputs is array (base_addresses'range) of std_logic_vector(base_addresses'range);
    
    -- inputs
    signal addr_test : std_logic_vector(C_BASE_ADDR'range) := x"0F0";
    signal read_en_test : std_logic := '1';
    signal write_en_test : std_logic := '1';
    -- outputs
--    signal addr_peri_test : std_logic_vector(base_addresses'range);
--    signal read_en_peri_test : std_logic := '0';
--    signal write_en_peri_test : std_logic := '0';
    signal read_array : rw_outputs;
    signal write_array : rw_outputs;
    signal addr_peri_array : addr_peri_outputs;
    
    
    
    -- procedure is basically a function that doesn't return anything
    procedure sim_message(msg : string) is 
        variable s : line;
    begin
        write (s, msg);
        writeline (output, s);
    end procedure;
    
    -- convert number (0 to 15) to hex string representation
    function int_to_hex(num: natural) return string is
        variable lut : string(1 to 16) := "0123456789ABCDEF";
    begin
        if num > 15 then
            return "";
        else
            return string'("" & lut(num+1));
        end if;
    end function;
    
    -- convert std_logic_vector to hex string representation
    -- TODO: write function that converts a std_logic_vector to hex string representation
    --       this wil also be a recursive function
    function vector_to_hex(vector: std_logic_vector) return string is
        variable copy : std_logic_vector(vector'high-4 downto 0);
        variable last : natural := 0;
    begin
        if (vector'length > 4) then
--            report "Value of current vector" & integer'image(TO_INTEGER(unsigned(vector)));
            last := TO_INTEGER(unsigned(vector(3 downto 0)));
            copy := vector(vector'length-1 downto 4);
            return string'(vector_to_hex(copy) & int_to_hex(last));
        else
--           report "Value of current vector" & integer'image(TO_INTEGER(unsigned(vector)));
           last := to_integer(unsigned(vector));
            return string'(int_to_hex(last));
        end if;
--        return string'(output);
    end function;
    
    -- convert std_logic_vector to binary string representation
    function to_bin_string(vector : std_logic_vector) return string is
    begin
        if vector(vector'high) = '0' then
            if vector'length = 1 then
                return "0";
            else
                return string'("0" & to_bin_string(vector(vector'high-1 downto 0)) ); -- recursive
            end if;
        else
            if vector'length = 1 then
                return "1";
            else
                return string'("1" & to_bin_string(vector(vector'high-1 downto 0)) ); -- recursive
            end if;
        end if;
    end function;
    
    -- component that will be tested
    component peripheral_address_decoder is
        generic(
                  C_BASE_ADDR : std_logic_vector := x"0F0"; 
            C_PERI_ADDR_WIDTH : natural := 1
        );
        port(
                     addr : in  std_logic_vector(C_BASE_ADDR'range);
                  read_en : in  std_logic;
                 write_en : in  std_logic;
                addr_peri : out std_logic_vector(C_PERI_ADDR_WIDTH-1 downto 0);
             read_en_peri : out std_logic;
            write_en_peri : out std_logic
        );
    end component;

    --debug information
    type debug_t is (resetting, invalid_addr, peripheral_test, ended);
    signal debug : debug_t;
    
    
begin
    dut_generation: for i in base_addresses'range generate
        -- DUT
        -- TODO: instantiate DUT
        pad : peripheral_address_decoder
        generic map (
            C_BASE_ADDR => base_addresses(i),
            C_PERI_ADDR_WIDTH => i+1
        )
        port map (
          addr => addr_test,
          read_en => read_en_test,
          write_en => write_en_test,
          addr_peri => addr_peri_array(i)(0 to i),
          write_en_peri => write_array(i),
          read_en_peri => read_array(i)
        );
    end generate dut_generation;
    
 

    CLK_PROC: process
    begin
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
        clk <= '0';
    end process CLK_PROC;

    STIM_PROC: process
    
    begin
        debug <= resetting;
        wait for clk_period;
        
	    --  TODO: write stim process
        for i in 1 to base_addresses'length loop
            for j in 0 to ((2 ** i)-1) loop
                sim_message("Test " & INTEGER'IMAGE(i) & "," & INTEGER'IMAGE(j));
                sim_message("C_PERRI_ADDR_WIDTH: " & integer'image(i));
                sim_message("C_BASE_ADDR: " & vector_to_hex(base_addresses(i-1)));
                sim_message("Current address: " & vector_to_hex(base_addresses(i-1)+j));
                sim_message("Expected output: " & to_bin_string(std_logic_vector(to_unsigned((2 ** (i-1)), 4))));
                
                addr_test <= base_addresses(i-1)+j;
                read_en_test <= '1';
                write_en_test <= '1';
--                assert C_ADDR_MASK'length = addr'length
--                report "C_ADDR_MASK and addr need to have the same length."
--                severity ERROR;
                assert read_array(i-1) = read_en_test
                report "Read signal was not let through"
                severity ERROR;
                
                assert write_array(i-1) = write_en_test
                report "Write signal was not let through"
                severity ERROR;
                
                sim_message("SUCCESS");
            end loop;
        end loop;
        -- end simulation
        
        debug <= ended;
        assert false -- will always execute
                report "SIMULATION ENDED"
                severity NOTE;
        
        wait;
            
    end process STIM_PROC;
    
    
end Behavioral;

