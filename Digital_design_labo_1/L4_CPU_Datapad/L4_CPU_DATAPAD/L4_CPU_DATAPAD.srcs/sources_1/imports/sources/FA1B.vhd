----------------------------------------------------------------------------------
-- Institution: KU Leuven
-- Students: Robin Verbeelen & Cas Vandendriessche
-- 
-- Module Name: FA1B - Behavioral
-- Course Name: Lab Digital Design
--
-- Description:
--  Full adder (1-bit)
--
----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY FA1B IS 
	PORT(
		-- TODO: complete entity declaration
		X: in std_logic;
		Y: in std_logic;
		carry_in1B: in std_logic;
		Z: out std_logic;
		carry_out1B: out std_logic
	);
END entity;

ARCHITECTURE LDD1 OF FA1B IS
	-- DONE: complete architecture
	signal xor1_result : std_logic := '0';
	signal xor2_result : std_logic := '0';
	signal and1_result : std_logic := '0';
	signal and2_result : std_logic := '0';
	signal or_result : std_logic := '0';

BEGIN
	xor1_result <= X xor Y;
	xor2_result <= xor1_result xor carry_in1B;
	and1_result <= xor1_result and carry_in1B;
	and2_result <= X and Y;
	or_result <= and1_result or and2_result;
	
	carry_out1B <= or_result;
	Z <= xor2_result;
END LDD1;

