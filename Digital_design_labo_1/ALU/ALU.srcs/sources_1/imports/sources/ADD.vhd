----------------------------------------------------------------------------------
-- Institution: KU Leuven
-- Students: Robin Verbeelen & Cas Vandendriessche
-- 
-- Module Name: ADD - Structural
-- Course Name: Lab Digital Design
--
-- Description:
--  n-bit ripple carry adder
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ADD is
	generic(
       C_DATA_WIDTH : natural := 4
	);
	port(
                a : in  std_logic_vector((C_DATA_WIDTH-1) downto 0); -- input var 1
                b : in  std_logic_vector((C_DATA_WIDTH-1) downto 0); -- input var 2
         carry_in : in  std_logic;                                   -- input carry
           result : out std_logic_vector((C_DATA_WIDTH-1) downto 0); -- alu operation result
        carry_out : out std_logic                                    -- carry
	);
end entity;

architecture LDD1 of ADD is
	-- DONE: list of signals and components

	-- signals
	signal carry: std_logic_vector(C_DATA_WIDTH downto 0);
	-- components
	COMPONENT FA1B
	PORT(
        X: in std_logic;
        Y: in std_logic;
        carry_in1B: in std_logic;
        Z: out std_logic;
        carry_out1B: out std_logic
    );
    END COMPONENT;

begin
	-- DONE: complete architecture description
	carry(0) <= carry_in;
	NBitRange:
	for I in 0 to C_DATA_WIDTH-1 generate
	   FA: FA1B PORT MAP (
	       X => a(i),
	       Y => b(i),
	       carry_in1B => carry(i),
	       Z => result(i),
	       carry_out1B => carry(i+1)
       );
    end generate NBitRange;
    carry_out <= carry(C_DATA_WIDTH);
end LDD1;
