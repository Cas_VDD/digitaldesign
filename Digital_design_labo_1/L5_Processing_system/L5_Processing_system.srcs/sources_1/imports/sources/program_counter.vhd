----------------------------------------------------------------------------------
-- Institution: KU Leuven
-- Students: Robin Verbeelen & Cas Vandendriesche
-- 
-- Module Name: program_counter - Behavioral
-- Course Name: Lab Digital Design
-- 
-- Description: 
--  An n-bit program counter module. The count step is set during instantiation.
--  A new count value can be loaded synchronously (le). Reset is asynchronous.
--  For a synchronous reset -> load "00..0"
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity program_counter is
    generic(
        C_PC_WIDTH : natural := 8;
         C_PC_STEP : natural := 2
    );
    port(
               clk : in  std_logic;
             reset : in  std_logic; -- async. reset
                up : in  std_logic; -- synch. count up
                le : in  std_logic; -- synch. load enable
             pc_in : in  std_logic_vector(C_PC_WIDTH-1 downto 0); -- parallel data in
            pc_out : out std_logic_vector(C_PC_WIDTH-1 downto 0)  -- parallel data out
    );
end program_counter;

architecture Behavioral of program_counter is
    -- DONE: (optionally) declare signals
    signal C_PC_STEP_vector : std_logic_vector(C_PC_WIDTH-1 downto 0);
    signal output : std_logic_vector(C_PC_WIDTH-1 downto 0) := (others => '0');
    
begin
    
    -- DONE: write VHDL process
    --reset  clk  le   up | pc_out (t+1)
    -- '1'    -    -   -  |    0
    -- '0'    re   -  '1' |  pc_out(t) + C_PC_STEP              re= rising edge
    -- '0'    re  '1' '0' |  pc_in
    -- '0'    re  '0' '0' | pc_out(t)
    
    C_PC_STEP_vector <= std_logic_vector(to_unsigned(C_PC_STEP, C_PC_STEP_vector'length));
    
    counter : process(clk, reset) is
    begin
        if reset = '1' then
            output <= (others => '0');
        elsif rising_edge(clk) then
            if up = '0' then
                if le = '1' then
                    output <= pc_in;
                    -- else not needed, don't expect change when le = 0
                end if;
            else
                output <= std_logic_vector(unsigned(output) + unsigned(C_PC_STEP_vector)); --vector addition
            end if;
        end if;
    end process counter;
    
    pc_out <= output;
    
end Behavioral;
