----------------------------------------------------------------------------------
-- Institution: KU Leuven
-- Students: Robin Verbeelen & Cas Vandendriessche
-- 
-- Module Name: register_file - Behavioral
-- Course Name: Lab Digital Design
--
-- Description:
--  Generic register file description. The number of registers and the data width
--  can be set with C_NR_REGS and C_DATA_WIDTH respectively.
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity register_file is
    generic(
        C_DATA_WIDTH : natural := 8;
           C_NR_REGS : natural := 8
    );
    port(
                 clk : in  std_logic;
               reset : in  std_logic;
                  le : in  std_logic;
              in_sel : in  std_logic_vector(C_NR_REGS-1 downto 0);
             out_sel : in  std_logic_vector(C_NR_REGS-1 downto 0);
             data_in : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
            data_out : out std_logic_vector(C_DATA_WIDTH-1 downto 0)
    );
end register_file;

architecture Behavioral of register_file is
    -- TODO: declare what will be used
    -- signal types
    type output_array is array (0 to C_NR_REGS-1) of std_logic_vector(C_DATA_WIDTH-1 downto 0);

    -- signals
    signal curr_reg_le : std_logic_vector(C_NR_REGS-1 downto 0) := (others => '0');
    signal zero_vector : std_logic_vector(C_DATA_WIDTH-1 downto 0) := (others => '0');
    
    signal register_outputs : output_array := (others=>(others=>'0'));
    signal mux_outputs : output_array := (others=>(others=>'0'));
    signal or_outputs : output_array := (others=>(others=>'0'));

    -- components
    component basic_register is
    generic (
        C_DATA_WIDTH : natural := 8
    );
    port(
                 clk : in  std_logic;
               reset : in  std_logic;
                  le : in  std_logic;
             data_in : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
            data_out : out std_logic_vector(C_DATA_WIDTH-1 downto 0)
    );
    end component;
begin
    -- TODO: describe how it's all connected and how it behaves
    GEN_Basic_registers:
    for I in 0 to C_NR_REGS-1 generate
        curr_reg_le(i) <= le and in_sel(i);
        with out_sel(i) select
            mux_outputs(i) <= register_outputs(i) WHEN '1',
                                   zero_vector WHEN others;
        B_REG : basic_register
            GENERIC MAP ( C_DATA_WIDTH => C_DATA_WIDTH )            
            PORT MAP (
                clk => clk,
                reset => reset,
                le => curr_reg_le(i),
                data_in => data_in,
                data_out => register_outputs(i)
            );
    end generate GEN_Basic_registers;
    
    or_outputs(C_NR_REGS-2) <= mux_outputs(C_NR_REGS-2) or mux_outputs(C_NR_REGS-1);
    
    GEN_Outputs:
    for I in 0 to C_NR_REGS-3 generate
        or_outputs(i) <= mux_outputs(i) or or_outputs(i+1);
    end generate GEN_Outputs;
    data_out <= or_outputs(0);
end Behavioral;
