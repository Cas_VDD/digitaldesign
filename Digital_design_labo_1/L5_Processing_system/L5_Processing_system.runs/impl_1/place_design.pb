
�
�No debug cores found in the current design.
Before running the implement_debug_core command, either use the Set Up Debug wizard (GUI mode)
or use the create_debug_core and connect_debug_core Tcl commands to insert debug cores into the design.
154*	chipscopeZ16-241h px� 
Q
Command: %s
53*	vivadotcl2 
place_design2default:defaultZ4-113h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2"
Implementation2default:default2
xc7a100t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2"
Implementation2default:default2
xc7a100t2default:defaultZ17-349h px� 
P
Running DRC with %s threads
24*drc2
42default:defaultZ23-27h px� 
V
DRC finished with %s
79*	vivadotcl2
0 Errors2default:defaultZ4-198h px� 
e
BPlease refer to the DRC report (report_drc) for more information.
80*	vivadotclZ4-199h px� 
p
,Running DRC as a precondition to command %s
22*	vivadotcl2 
place_design2default:defaultZ4-22h px� 
P
Running DRC with %s threads
24*drc2
42default:defaultZ23-27h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2t
 "^
#ROM_INST/do_reg_reg/ADDRARDADDR[10]#ROM_INST/do_reg_reg/ADDRARDADDR[10]2default:default2default:default2H
 "2
ROM_INST/Q[6]ROM_INST/Q[6]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[6]	"PROCESSOR_INST/MAR/data_out_reg[6]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[4]"ROM_INST/do_reg_reg/ADDRARDADDR[4]2default:default2default:default2H
 "2
ROM_INST/Q[0]ROM_INST/Q[0]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[0]	"PROCESSOR_INST/MAR/data_out_reg[0]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[5]"ROM_INST/do_reg_reg/ADDRARDADDR[5]2default:default2default:default2H
 "2
ROM_INST/Q[1]ROM_INST/Q[1]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[1]	"PROCESSOR_INST/MAR/data_out_reg[1]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[6]"ROM_INST/do_reg_reg/ADDRARDADDR[6]2default:default2default:default2H
 "2
ROM_INST/Q[2]ROM_INST/Q[2]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[2]	"PROCESSOR_INST/MAR/data_out_reg[2]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[7]"ROM_INST/do_reg_reg/ADDRARDADDR[7]2default:default2default:default2H
 "2
ROM_INST/Q[3]ROM_INST/Q[3]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[3]	"PROCESSOR_INST/MAR/data_out_reg[3]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[8]"ROM_INST/do_reg_reg/ADDRARDADDR[8]2default:default2default:default2H
 "2
ROM_INST/Q[4]ROM_INST/Q[4]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[4]	"PROCESSOR_INST/MAR/data_out_reg[4]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2r
 "\
"ROM_INST/do_reg_reg/ADDRARDADDR[9]"ROM_INST/do_reg_reg/ADDRARDADDR[9]2default:default2default:default2H
 "2
ROM_INST/Q[5]ROM_INST/Q[5]2default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[5]	"PROCESSOR_INST/MAR/data_out_reg[5]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[0]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[0]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[1]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[1]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[2]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[2]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[3]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[3]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[4]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[4]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2�
 "p
,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[5]	,PROCESSOR_INST/CU/ctrl_mem_addr_i_reg_rep[5]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
�
�RAMB18 async control check: The RAMB18E1 %s has an input control pin %s (net: %s) which is driven by a register (%s) that has an active asychronous set or reset. This may cause corruption of the memory contents and/or read values when the set/reset is asserted and is not analyzed by the default static timing analysis. It is suggested to eliminate the use of a set/reset to registers driving this RAMB pin or else use a synchronous reset in which the assertion of the reset is timed by default.%s*DRC2T
 ">
ROM_INST/do_reg_reg	ROM_INST/do_reg_reg2default:default2default:default2d
 "N
ROM_INST/do_reg_reg/ENARDENROM_INST/do_reg_reg/ENARDEN2default:default2default:default2�
 "t
.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_3.ROM_INST/do_reg_reg_ENARDEN_cooolgate_en_sig_32default:default2default:default2r
 "\
"PROCESSOR_INST/MAR/data_out_reg[7]	"PROCESSOR_INST/MAR/data_out_reg[7]2default:default2default:default2B
 *DRC|Netlist|Instance|Required Pin|RAMB18E12default:default8Z	REQP-1840h px� 
c
DRC finished with %s
79*	vivadotcl2)
0 Errors, 14 Warnings2default:defaultZ4-198h px� 
e
BPlease refer to the DRC report (report_drc) for more information.
80*	vivadotclZ4-199h px� 
U

Starting %s Task
103*constraints2
Placer2default:defaultZ18-103h px� 
}
BMultithreading enabled for place_design using a maximum of %s CPUs12*	placeflow2
42default:defaultZ30-611h px� 
v

Phase %s%s
101*constraints2
1 2default:default2)
Placer Initialization2default:defaultZ18-101h px� 
�

Phase %s%s
101*constraints2
1.1 2default:default29
%Placer Initialization Netlist Sorting2default:defaultZ18-101h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:00.012default:default2
2247.4652default:default2
0.0002default:default2
20252default:default2
124862default:defaultZ17-722h px� 
Z
EPhase 1.1 Placer Initialization Netlist Sorting | Checksum: ba8092cf
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:00.01 ; elapsed = 00:00:00.08 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2025 ; free virtual = 124862default:defaulth px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
2247.4652default:default2
0.0002default:default2
20252default:default2
124862default:defaultZ17-722h px� 
�

Phase %s%s
101*constraints2
1.2 2default:default2F
2IO Placement/ Clock Placement/ Build Placer Device2default:defaultZ18-101h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
g
RPhase 1.2 IO Placement/ Clock Placement/ Build Placer Device | Checksum: 76e07ee2
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:01 ; elapsed = 00:00:00.86 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2013 ; free virtual = 124782default:defaulth px� 
}

Phase %s%s
101*constraints2
1.3 2default:default2.
Build Placer Netlist Model2default:defaultZ18-101h px� 
O
:Phase 1.3 Build Placer Netlist Model | Checksum: 84712e10
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:02 ; elapsed = 00:00:01 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2008 ; free virtual = 124742default:defaulth px� 
z

Phase %s%s
101*constraints2
1.4 2default:default2+
Constrain Clocks/Macros2default:defaultZ18-101h px� 
L
7Phase 1.4 Constrain Clocks/Macros | Checksum: 84712e10
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:02 ; elapsed = 00:00:01 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2008 ; free virtual = 124742default:defaulth px� 
H
3Phase 1 Placer Initialization | Checksum: 84712e10
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:02 ; elapsed = 00:00:01 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2008 ; free virtual = 124742default:defaulth px� 
q

Phase %s%s
101*constraints2
2 2default:default2$
Global Placement2default:defaultZ18-101h px� 
C
.Phase 2 Global Placement | Checksum: d572ba5e
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:06 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1994 ; free virtual = 124612default:defaulth px� 
q

Phase %s%s
101*constraints2
3 2default:default2$
Detail Placement2default:defaultZ18-101h px� 
}

Phase %s%s
101*constraints2
3.1 2default:default2.
Commit Multi Column Macros2default:defaultZ18-101h px� 
O
:Phase 3.1 Commit Multi Column Macros | Checksum: d572ba5e
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:06 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1994 ; free virtual = 124612default:defaulth px� 


Phase %s%s
101*constraints2
3.2 2default:default20
Commit Most Macros & LUTRAMs2default:defaultZ18-101h px� 
Q
<Phase 3.2 Commit Most Macros & LUTRAMs | Checksum: ff22c522
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:06 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124602default:defaulth px� 
y

Phase %s%s
101*constraints2
3.3 2default:default2*
Area Swap Optimization2default:defaultZ18-101h px� 
L
7Phase 3.3 Area Swap Optimization | Checksum: 14417495c
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:06 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124592default:defaulth px� 
�

Phase %s%s
101*constraints2
3.4 2default:default22
Pipeline Register Optimization2default:defaultZ18-101h px� 
T
?Phase 3.4 Pipeline Register Optimization | Checksum: 14417495c
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:06 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124592default:defaulth px� 


Phase %s%s
101*constraints2
3.5 2default:default20
Small Shape Detail Placement2default:defaultZ18-101h px� 
R
=Phase 3.5 Small Shape Detail Placement | Checksum: 1448270ba
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124602default:defaulth px� 
u

Phase %s%s
101*constraints2
3.6 2default:default2&
Re-assign LUT pins2default:defaultZ18-101h px� 
H
3Phase 3.6 Re-assign LUT pins | Checksum: 118ff28d0
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124602default:defaulth px� 
�

Phase %s%s
101*constraints2
3.7 2default:default22
Pipeline Register Optimization2default:defaultZ18-101h px� 
T
?Phase 3.7 Pipeline Register Optimization | Checksum: 118ff28d0
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124602default:defaulth px� 
D
/Phase 3 Detail Placement | Checksum: 118ff28d0
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1993 ; free virtual = 124602default:defaulth px� 
�

Phase %s%s
101*constraints2
4 2default:default2<
(Post Placement Optimization and Clean-Up2default:defaultZ18-101h px� 
{

Phase %s%s
101*constraints2
4.1 2default:default2,
Post Commit Optimization2default:defaultZ18-101h px� 
E
%Done setting XDC timing constraints.
35*timingZ38-35h px� 
�

Phase %s%s
101*constraints2
4.1.1 2default:default2/
Post Placement Optimization2default:defaultZ18-101h px� 
V
APost Placement Optimization Initialization | Checksum: 14e44528d
*commonh px� 
u

Phase %s%s
101*constraints2
4.1.1.1 2default:default2"
BUFG Insertion2default:defaultZ18-101h px� 
�
EMultithreading enabled for phys_opt_design using a maximum of %s CPUs380*physynth2
42default:defaultZ32-721h px� 
�
�BUFG insertion identified %s candidate nets, %s success, %s skipped for placement/routing, %s skipped for timing, %s skipped for netlist change reason.30*	placeflow2
02default:default2
02default:default2
02default:default2
02default:default2
02default:defaultZ46-31h px� 
H
3Phase 4.1.1.1 BUFG Insertion | Checksum: 14e44528d
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1994 ; free virtual = 124622default:defaulth px� 
�
hPost Placement Timing Summary WNS=%s. For the most accurate timing information please run report_timing.610*place2
70.4552default:defaultZ30-746h px� 
S
>Phase 4.1.1 Post Placement Optimization | Checksum: 155bf5197
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1994 ; free virtual = 124622default:defaulth px� 
N
9Phase 4.1 Post Commit Optimization | Checksum: 155bf5197
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1994 ; free virtual = 124622default:defaulth px� 
y

Phase %s%s
101*constraints2
4.2 2default:default2*
Post Placement Cleanup2default:defaultZ18-101h px� 
L
7Phase 4.2 Post Placement Cleanup | Checksum: 155bf5197
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1995 ; free virtual = 124632default:defaulth px� 
s

Phase %s%s
101*constraints2
4.3 2default:default2$
Placer Reporting2default:defaultZ18-101h px� 
F
1Phase 4.3 Placer Reporting | Checksum: 155bf5197
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1995 ; free virtual = 124632default:defaulth px� 
z

Phase %s%s
101*constraints2
4.4 2default:default2+
Final Placement Cleanup2default:defaultZ18-101h px� 
M
8Phase 4.4 Final Placement Cleanup | Checksum: 191b51cb8
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1995 ; free virtual = 124632default:defaulth px� 
\
GPhase 4 Post Placement Optimization and Clean-Up | Checksum: 191b51cb8
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1995 ; free virtual = 124632default:defaulth px� 
=
(Ending Placer Task | Checksum: e3c9e511
*commonh px� 
�

%s
*constraints2�
�Time (s): cpu = 00:00:07 ; elapsed = 00:00:03 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2006 ; free virtual = 124732default:defaulth px� 
Z
Releasing license: %s
83*common2"
Implementation2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
552default:default2
142default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
place_design2default:defaultZ4-42h px� 
D
Writing placer database...
1603*designutilsZ20-1893h px� 
=
Writing XDEF routing.
211*designutilsZ20-211h px� 
J
#Writing XDEF routing logical nets.
209*designutilsZ20-209h px� 
J
#Writing XDEF routing special nets.
210*designutilsZ20-210h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2)
Write XDEF Complete: 2default:default2
00:00:00.172default:default2
00:00:00.062default:default2
2247.4652default:default2
0.0002default:default2
20032default:default2
124732default:defaultZ17-722h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�/home/student/2021-digitaldesignlabo-casnrobin/Digital_design_labo_1/L5_Processing_system/L5_Processing_system.runs/impl_1/top_placed.dcp2default:defaultZ17-1381h px� 
^
%s4*runtcl2B
.Executing : report_io -file top_io_placed.rpt
2default:defaulth px� 
�
�report_io: Time (s): cpu = 00:00:00.11 ; elapsed = 00:00:00.13 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 1997 ; free virtual = 12465
*commonh px� 
�
%s4*runtcl2r
^Executing : report_utilization -file top_utilization_placed.rpt -pb top_utilization_placed.pb
2default:defaulth px� 
�
�report_utilization: Time (s): cpu = 00:00:00.07 ; elapsed = 00:00:00.11 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2003 ; free virtual = 12471
*commonh px� 
{
%s4*runtcl2_
KExecuting : report_control_sets -verbose -file top_control_sets_placed.rpt
2default:defaulth px� 
�
�report_control_sets: Time (s): cpu = 00:00:00.07 ; elapsed = 00:00:00.08 . Memory (MB): peak = 2247.465 ; gain = 0.000 ; free physical = 2003 ; free virtual = 12471
*commonh px� 


End Record